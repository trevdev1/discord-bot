"""
Simple discord bot to practice async/await.
"""
import asyncio
import json
import os
from random import choice, randint

from asyncpraw import Reddit
from discord.ext import commands
from discord.ext.commands.context import Context
import yaml


GUILD = os.getenv("GUILD_NAME")

with open("resources/links.yml") as links:
    LINKS = yaml.safe_load(links)

with open("resources/copypastas.yml") as pastas:
    PASTAS = yaml.safe_load(pastas)

bot = commands.Bot(command_prefix="$")

reddit = Reddit(
    client_id=os.getenv("REDDIT_CLIENT_ID"),
    client_secret=os.getenv("REDDIT_CLIENT_SECRET"),
    user_agent="test user agent",
)


@bot.event
async def on_ready():
    """
    Runs once the bot is connected to Discord.
    """
    print(f"{bot.user.name} has connected to Discord!")
    for guild in bot.guilds:
        print(f"Connected to {guild} (id: {guild.id})")


@bot.event
async def on_message(message):
    """
    Runs every time a message is received
    """
    if message.author != bot.user and "based" in message.content.lower():
        msg = await message.channel.send(PASTAS["based"])
        await asyncio.sleep(7)
        await msg.delete()

    await bot.process_commands(message)


@bot.command(
    name="clean",
    help="Removes all bot commands and bot-sent messages from the channel.",
)
async def clean(ctx: Context):
    """
    Cleans the recent messages in the channel.
    Deletes all messages sent by the bot, and any commands
    that were given to the bot.
    """
    msg = await ctx.send("Cleaning...")
    to_delete = [
        message
        async for message in ctx.channel.history(limit=50, before=msg.created_at)
        if message.author == bot.user
        or len(message.content) > 1
        and message.content[0] == bot.command_prefix
        and message.content.split()[0][1:] in bot.all_commands
    ]

    await ctx.channel.delete_messages(to_delete)
    await msg.edit(
        content=f"Cleaned `{len(to_delete) - 1}` messages. :put_litter_in_its_place:"
    )
    await asyncio.sleep(1)
    await msg.delete()


@bot.command(name="jerma")
async def jerma_post(ctx: Context):
    """Posts jerma stuff duh"""
    await ctx.send(choice(list(LINKS["jerma"].values())))


@bot.command(name="reload")
async def reload():
    """
    Reloads the bot's configuration files
    """

    with open("resources/links.yml") as links:
        LINKS.update(yaml.load(links, yaml.SafeLoader))


async def scrape_reddit_linkpost(subreddit):
    """
    Grabs a random linkpost off of the given subreddit
    """

    sub = await reddit.subreddit(subreddit)
    post = await sub.random()
    # retry until we get a link post
    while post.selftext != "":
        post = await sub.random()
    return post.url


@bot.command(name="jarma")
async def jerma_reddit(ctx: Context):
    """
    Grabs a random post from the jerma985 subreddit and posts it to discord.
    """
    await ctx.send(await scrape_reddit_linkpost("jerma985"))


@bot.command(name="tf2")
async def okay_buddy_fortress(ctx: Context):
    """
    Grabs a random post from the okaybuddyfortress subreddit and posts it to discord.
    """
    await ctx.send(
        await scrape_reddit_linkpost(choice(("okaybuddyfortress", "tf2shitposterclub")))
    )


@bot.command(name="brit")
async def ok_mate(ctx: Context):
    """
    Grabs a random post from the okmatewanker subreddit and posts it to discord.
    """
    await ctx.send(await scrape_reddit_linkpost("okmatewanker"))


bot.run(os.getenv("DISCORD_BOT_TOKEN"))
